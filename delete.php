<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// [1]
$database = LEPTON_database::getInstance();

// [2] Remove chapter access files
$query_details = [];
$database->execute_query(
    "SELECT * FROM ".TABLE_PREFIX."mod_manual_chapters WHERE section_id = '$section_id'",
    true,
    $query_details,
    true
);

if (!empty($query_details))
{
    foreach($query_details as $get_details)
    {
        // Unlink chapter access file
        if (is_writable(LEPTON_PATH . PAGES_DIRECTORY . $get_details['link'] . PAGE_EXTENSION))
        {
            unlink(LEPTON_PATH . PAGES_DIRECTORY . $get_details['link'] . PAGE_EXTENSION);
        }
    }
}
// Remove chapter sub dirs second level files
$query_details = [];
$database->execute_query(
    "SELECT * FROM ".TABLE_PREFIX."mod_manual_chapters WHERE section_id = ".$section_id." AND level = 2 ",
    true,
    $query_details,
    true
);

if (!empty($query_details))
{
    foreach($query_details as $get_details) {

        // Unlink chapter access dir
        if (is_writable(LEPTON_PATH . PAGES_DIRECTORY . $get_details['link'])) {
            rmdir(LEPTON_PATH . PAGES_DIRECTORY . $get_details['link']);
        }
    }
}
// Remove chapter sub dirs first level files
$query_details = [];
$database->execute_query(
    "SELECT * FROM ".TABLE_PREFIX."mod_manual_chapters WHERE section_id = ".$section_id." AND level = 1 ",
    true,
    $query_details,
    true
);

if (!empty($query_details))
{
    foreach($query_details as $get_details) {
        // Unlink chapter access dir
        if (is_writable(LEPTON_PATH . PAGES_DIRECTORY . $get_details['link']))
        {
            rmdir(LEPTON_PATH . PAGES_DIRECTORY . $get_details['link']);
        }
    }
}

// Delete records from the database
$database->simple_query("DELETE FROM " . TABLE_PREFIX . "mod_manual_settings WHERE section_id = " . $section_id);
$database->simple_query("DELETE FROM " . TABLE_PREFIX . "mod_manual_chapters WHERE section_id = " . $section_id);
