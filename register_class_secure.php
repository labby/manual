<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

$files_to_register = [
	"add_chapter.php",	
	"add.php",
	"delete_chapter.php",
	"delete.php",
	"install.php",
	"modify_chapter.php",
	"modify_settings.php",
	"save_chapter.php",
	"save_settings.php",
	"uninstall.php",
	"update_chapter_state.php",
	"update_chapter_positions.php",
	"view.php"
];

LEPTON_secure::getInstance()->accessFiles($files_to_register);
