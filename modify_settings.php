<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


$oMANUAL = manual::getInstance();

// Include admin wrapper script
$update_when_modified = true;
require(LEPTON_PATH.'/modules/admin.php');

// Get content from the db
$fetch_content = array();
$query_content = $database->execute_query(
	"SELECT `header`,`footer` FROM `".TABLE_PREFIX."mod_manual_settings` WHERE `section_id` = ".$section_id,
	true,
	$fetch_content,
	false
);


//  CSS edit
LEPTON_handle::register("mod_file_exists", "edit_module_css");
ob_start();
   edit_module_css('manual');
   $called_edit_module_css = ob_get_clean();
   

//  Render   
$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule("manual");

echo $oTWIG->render(
    "@manual/backend/modify_settings.lte",
    [
        "oMANUAL"       => $oMANUAL,
        "page_id"       => $page_id,
        "section_id"    => $section_id,
        "header"    	=> $fetch_content["header"],
        "footer"    	=> $fetch_content["footer"],
        "module_css"    => $called_edit_module_css
    ]
);

// Print admin footer
$admin->print_footer();
