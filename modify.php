<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


global $page_id, $section_id;
$database = LEPTON_database::getInstance();

//	removes empty entries from the table so they will not be displayed
//		Aldus: 2017-04-27 - Mit einem Fuß im Grab!
$database->simple_query(
	"DELETE FROM `".TABLE_PREFIX."mod_manual_chapters` WHERE `page_id` = '" . $page_id. "' and title=''"
);

//  Eintrag in der Session? löschen!
if (isset($_SESSION['manual_chapter_last_values']))
{
    unset($_SESSION['manual_chapter_last_values']);
}
    
/**
 *	Get the template engine
 */
$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule('manual');

/**
 *	Get the manual data
 */
$oMANUAL = manual::getInstance();
$all_chapters = $oMANUAL->get_manual_by_sectionID( $section_id );

$chapter_tree = [];
$oMANUAL->build_backend_tree($all_chapters, $chapter_tree, 0);

$page_values = [
	'chapter_tree'	=> $chapter_tree,
    "section_id"	=> $section_id,
    "page_id"		=> $page_id,
    "leptoken"		=> get_leptoken(),
    "oMANUAL"		=> $oMANUAL,
    "image_url"     => LEPTON_URL."/modules/lib_lepton/backend_images"
];

echo $oTWIG->render(
	"@manual/backend/modify.lte",
	$page_values
);

