/**
 *	manual
 *	backend body script
 */

/**
 *  Needed for the $.sortable/drag as callback! 
 */
var xmlhttp;
if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
} else {
    // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}

function manual_update_position( sString, iSectionID ) {
    var iMySectionId;
    iMySectionId = iSectionID;
    
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4) {
            if (xmlhttp.status==200) {

                // console.log ("Response: "+xmlhttp.responseText);

                var message_box_message_ref = document.getElementById("manual_ajax_response_text_"+iMySectionId);
                message_box_message_ref.innerHTML = xmlhttp.responseText;

                var message_box_ref = document.getElementById("manual_ajax_response_"+iMySectionId);
                message_box_ref.style.display = "inline-block";

                return true;
                // alert ("Response: "+xmlhttp.responseText);
            }
        }
    }

    xmlhttp.open("POST",LEPTON_URL+"/modules/manual/update_chapter_positions.php", true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("chapters="+sString+"&sig=axi023");
}

/**
 *  Update the chapter state
 */
function manual_set_state( aRef, aID, iSectionID )
{
    var iMySectionId;
    iMySectionId = iSectionID;

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4) {
            if (xmlhttp.status==200) {

                // console.log ("Response: "+xmlhttp.responseText);

                var message_box_message_ref = document.getElementById("manual_ajax_response_text_"+iMySectionId);
                message_box_message_ref.innerHTML = xmlhttp.responseText;

                var message_box_ref = document.getElementById("manual_ajax_response_"+iMySectionId);
                message_box_ref.style.display = "inline-block";

                /**
                 *  Path
                 */
                var aBasepath = aRef.src.split("/");

                var terms = xmlhttp.responseText.split("~");
                if(terms[1] == 1)
                {
                    // console.log("path= "+aRef.src );
                    aBasepath[ aBasepath.length -1 ] = "view_16.png";
                } else {
                    aBasepath[ aBasepath.length -1 ] = "none_16.png";
                }
                aRef.src = aBasepath.join("/");

                message_box_message_ref.innerHTML = terms[0];
                return true;
                // alert ("Response: "+xmlhttp.responseText);
            }
        }
    }

    xmlhttp.open("POST",LEPTON_URL+"/modules/manual/update_chapter_state.php", true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("cid="+aID+"&sig=aix5641");
}

/**
 *  Delete a chapter
 */
function manual_delete_chapter( aMessage, url ) {
    
    if(true === confirm( unescape(decodeURI(aMessage)) ))
    {
        //alert("jo");
        location.href = url + "&amp;leptokh=#-!leptoken-!#";
    }
}