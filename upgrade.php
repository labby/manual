<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


//  1.0 Delete obsolete files
$file_names = array(
	'/modules/manual/backend.css',
	'/modules/manual/backend.js',
	'/modules/manual/frontend.js',
	'/modules/manual/frontend.css',
	'/modules/manual/move_down.php',
	'/modules/manual/move_up.php',
	'/modules/manual/update_manual.php',
);
LEPTON_handle::delete_obsolete_files ($file_names);

//  2.0 Update db fields
$aTableInfo = [];
$bSuccess = $database->describe_table( TABLE_PREFIX."mod_manual_chapters", $aTableInfo );

//  2.1 get table info
if(false === $bSuccess)
{
    die( LEPTON_tools::display( $database->get_error(), "pre", "ui message red") );

} else {
    
    // 2.2 look for specific fields    
    $bTagFound = false;
    $bSeeAlsoFound = false;
    $bTypeFound = false;
    
    foreach( $aTableInfo as $field)
    {
        switch($field["Field"])
        {
            case "tags":
                $bTagFound = true;
                break;
        
            case "see_also":
                $bSeeAlsoFound = true;
                break;
            
            case "type":
                $bTypeFound = true;
                break;
                
            default:
                // nothing
        }
    }

    //  2.3 try to update the table
    $aErrors = [];
    
    //  2.3.1 tags
    if(false === $bTagFound)
    {
        $bSuccess = $database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_manual_chapters` ADD `tags` TEXT  NULL  AFTER `modified_by`;" );
        if(false === $bSuccess)
        {
            $aErrors[] = $database->get_error();
        }
    }
    
    //  2.3.2 see also
    if(false === $bSeeAlsoFound)
    {
        $bSuccess = $database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_manual_chapters` ADD `see_also` varchar(255) DEFAULT '' AFTER `tags`;" );
        if(false === $bSuccess)
        {
            $aErrors[] = $database->get_error();
        }
    }
    
    //  2.3.3 type
    if(false === $bTypeFound)
    {   
        $bSuccess = $database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_manual_chapters` ADD `type` INT  NOT NULL  DEFAULT '1'  AFTER `see_also`;" );
        if(false === $bSuccess)
        {
            $aErrors[] = $database->get_error();
        }
    }

    //  2.4 - any Errors to display
    if(count($aErrors) > 0)
    {
        die( LEPTON_tools::display( 
            "<p>".implode("<br />", $aErrors)."</p>",
            "pre",
            "ui message red"
        ));
    }
}
