<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

class manual_signature
{

    const MANUAL_REMOVALS = ["REQUEST_TIME_FLOAT", "REQUEST_TIME"];

    /**
     * @param array $aArray
     * @return string
     */
    static public function nestedImplode(array $aArray): string
    {
        $s = "";
        foreach ($aArray as $ref)
        {
            if (is_array($ref))
            {
                $s .= self::nestedImplode($ref);
            } else {
                $s .= $ref;
            }
        }
        return $s;
    }
    
    static public function getSignature()
    {
        $aServer = $_SERVER;
        foreach (self::MANUAL_REMOVALS as $key)
        {
            if(isset($aServer[ $key ]))
            {
                unset($aServer[ $key ]);
            }
        }
        return sha1( self::nestedImplode($aServer) . LEPTON_GUID );
    }
}