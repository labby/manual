<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

class manual extends LEPTON_abstract
{
	/**
     * The singleton-instance of this class
     *
     * @var $instance
     */
    public static $instance;
    
    /**
     * To avoid doubles in the frontend
     * @var bool
     */
    public bool $detail_shown = false;
    
    public function initialize()
    {
        // from parent
    }

    /**
     * @param int $iSecId
     * @param bool $bShowAll
     * @return array
     */
    public function get_manual_by_sectionID(int $iSecId = 0, bool $bShowAll = true): array
    {
    	$show_only_actives = ($bShowAll === true) ? "" : "AND `active`=1";
 
    	$database = LEPTON_database::getInstance();
    	$all = [];
    	$database->execute_query(
    		"SELECT * FROM `".TABLE_PREFIX."mod_manual_chapters` WHERE `section_id`=".$iSecId." ".$show_only_actives." ORDER BY `parent`,`position`",
    		true,
    		$all,
    		true
    	);
    	
    	$aRetVal = [];
        foreach ($all as &$ref)
    	{
            $aRetVal[$ref['chapter_id']] = $ref;
    	}
    	
    	return $aRetVal;
    }

    /**
     * @param string $sPath
     * @return void
     */
    public function test_root(string $sPath=""): void
    {
    	// $full_filepath = LEPTON_PATH.PAGES_DIRECTORY.$page_link.$temp_root."/";
        $basepath = LEPTON_PATH . PAGES_DIRECTORY;
    	$sub_part = str_replace( LEPTON_PATH.PAGES_DIRECTORY."/", "", $sPath);
    	$elements = explode("/", $sub_part);
        foreach ($elements as $folder)
        {
    		$basepath .= "/" . $folder;
            if (!file_exists($basepath))
    		{
                if (true === mkdir($basepath, 0755))
    			{
    				copy(
                        LEPTON_PATH . "/backend/pages/master_index.php",
                        $basepath . "/index.php"
    				);
    			}
    		}
    	}
    }

    /**
     * @param array $allChapters
     * @param int $aChapterID
     * @return string
     */
    public function get_root(array &$allChapters, int $aChapterID): string
    {
    	$root = "";

        if (!isset($allChapters[$aChapterID]))
    	{
    		return $root;
    	}
    	
    	do
    	{
            $parent = $allChapters[$aChapterID]['parent'];
            $root = $allChapters[$aChapterID]['link'] . $root;
    		$aChapterID = $parent;
    	} while ($parent != 0);
    	
    	return $root;
    }

    /**
     * @param array $allChapters
     * @param int $aChapterID
     * @return array
     */
    public function get_sub_chapters(array &$allChapters, int $aChapterID): array
    {
    	$returnVal = [];
        foreach ($allChapters as $key => $data)
        {
    		if ($data['parent'] === $aChapterID)
    		{
                $returnVal[$key] = $data;
    		}
    	}
   		return $returnVal; 
    }

    /**
     * @param int $aParentID
     * @return array
     */
    public function get_siblings(int $aParentID = 0): array
	{
		$database = LEPTON_database::getInstance();
		$all = [];
		$database->execute_query(
			"SELECT `chapter_id`,`link`,`position`,`title` FROM `".TABLE_PREFIX."mod_manual_chapters` WHERE `parent`=".$aParentID." AND `active`='1' ORDER BY `position`",
			true,
			$all,
			true
		);
		return $all;
	}

    /**
     * @param array $allChapters
     * @param array $aTreeStorage
     * @param int $aChapterID
     * @return void
     */
    public function build_tree(array &$allChapters, array &$aTreeStorage=array() , int $aChapterID=0): void
	{
		$oLEPTON = LEPTON_frontend::getInstance();

        foreach ($allChapters as $key => $currentChapter)
		{
            if ($currentChapter['parent'] == $aChapterID)
			{
				//	Build the complete link incl. the "root"
                $currentChapter['link'] = $this->page_link($oLEPTON->page['link'] . $this->get_root($allChapters, $key));
				
				//	get subchapters
				$currentChapter['subchapters'] = [];

                $aTreeStorage[$key] = $currentChapter;

                $sub_chapters = $this->get_sub_chapters($allChapters, $currentChapter['chapter_id']);
                foreach ($sub_chapters as $subkey => $subdata)
                {
                    $subdata['link'] = $this->page_link($oLEPTON->page['link'] . $this->get_root($allChapters, $subkey));
					$subdata['subchapters']	= [];

                    $this->build_tree($allChapters, $subdata['subchapters'], $subdata['chapter_id']);

                    $aTreeStorage[$key]['subchapters'][$subkey] = $subdata;
				}
			}
		}
	}	
	
	/**
	 *	The backend version for the (chapter-)tree
	 *
	 *	@param	array	$allChapters A given array within all chapters as a linea list, pass by reference!
	 *	@param	array	$aTreeStorage A given storage for the tree (or sub-tree), pass by reference
	 *	@param	integer $aChapterID The "root" chapter we are looking for.
	 *
	 *	@return	void	As all storages are called by reference.
	 *
	 */
    public function build_backend_tree(array &$allChapters, array &$aTreeStorage = [], int $aChapterID = 0): void
	{
		$MOD_MANUAL = $this->language;
		
		foreach($allChapters as $key => $currentChapter)
		{
			if($currentChapter['parent'] == $aChapterID)
			{

                $currentChapter['title'] = strip_tags($currentChapter['title'], "");

                $currentChapter['are_you_sure'] = sprintf($MOD_MANUAL['ARE_YOU_SURE'], $currentChapter['title']);
				
				//	get subchapters
				$currentChapter['subchapters'] = [];

                $aTreeStorage[$key] = $currentChapter;

                $sub_chapters = $this->get_sub_chapters($allChapters, $currentChapter['chapter_id']);
                foreach ($sub_chapters as $subkey => $subdata)
                {
					$subdata['subchapters']	= [];
                    $subdata['are_you_sure'] = sprintf($MOD_MANUAL['ARE_YOU_SURE'], $subdata['title']);

                    $this->build_backend_tree($allChapters, $subdata['subchapters'], $subdata['chapter_id']);

                    $aTreeStorage[$key]['subchapters'][$subkey] = $subdata;
				}
			}
		}
	}
	
	/**
	 *  Build the complete <a> tag (with href) for a given chapter-id.
	 *
	 *  @param  int $aChapterID  A given chapter-ID.
	 *
	 *  @return string  The generated a tag.
	 *
	 */
    public function get_root_link(int $aChapterID = 0): string
	{
        if ($aChapterID === 0)
        {
            return "";
        }
		
		$database = LEPTON_database::getInstance();
		
		$chapter = [];
		$database->execute_query(
			"SELECT * FROM `".TABLE_PREFIX."mod_manual_chapters` WHERE `chapter_id`	= ".$aChapterID,
			true,
			$chapter,
			false
		);
		
		if (empty($chapter))
        {
            return "";
        }

        $page_root_link = $database->get_one("SELECT `link` FROM `" . TABLE_PREFIX . "pages` WHERE `page_id`=" . $chapter['page_id']);

        $all = $this->get_manual_by_sectionID($chapter['section_id'], false);

        $root = $this->get_root($all, $aChapterID);

        $full_url = LEPTON_URL . PAGES_DIRECTORY . $page_root_link . $root . ".php";

        return "<a href='" . $full_url . "'>" . $chapter['title'] . "</a>";
	}
	
	/**
	 *  Parse a given string for the [code] tag
	 *
	 *  @param  string  $sAnyString  Any valid string. Pass by reference!
	 *  @return void As param is passed by reference!
	 *
	 */
	public static function parseStr(string &$sAnyString): void
	{
		$sAnyString = str_replace(
			["[code]", "[/code]"],
			["<code class='manual_code'>", "</code>"],
			$sAnyString
		);
	}
	
	public function getAllChapters()
	{
	    $database = LEPTON_database::getInstance();
	    
	    $aAllChapters = [];
	    $bResult = $database->execute_query(
	        "SELECT 
	                `chapter_id`, `title`, `page_id`, `section_id`,`parent`,`position`
	            FROM
	                `".TABLE_PREFIX."mod_manual_chapters`
	            ORDER BY
	                `page_id`,`section_id`,`parent`,`position`",
	        true,
	        $aAllChapters,
	        true
	    );
	    
	    if (false === $bResult)
	    {
            die(LEPTON_tools::display($database->get_error()));
	    }
	    
	    $aResult = [];
	    foreach ($aAllChapters as &$aTempChapter)
	    {
            if (!isset($aResult[$aTempChapter['page_id']]))
	        {
	            $aResult[ $aTempChapter['page_id'] ] = [
	                'title' => $database->get_one("SELECT `menu_title` FROM `".TABLE_PREFIX."pages` WHERE `page_id`=".$aTempChapter['page_id'] ),
	                'sections'  => []
	            ];
	        }

            if (!isset($aResult[$aTempChapter['page_id']]['sections'][$aTempChapter['section_id']]))
	        {
	            $aResult[ $aTempChapter['page_id'] ]['sections'][ $aTempChapter['section_id'] ] = [
	                'name' => $database->get_one("SELECT `name` FROM `".TABLE_PREFIX."sections` WHERE `section_id` = ".$aTempChapter['section_id']),
	                'chapters' => []
	            ];
	        }

            $aResult[$aTempChapter['page_id']]['sections'][$aTempChapter['section_id']]['chapters'] [] = $aTempChapter;
	    }
	    
	    return $aResult; // $aAllChapters;
	}
	
	/**
	 *  Returns an assoc. array with some settings from the db entry for a given section(-id).
	 *
	 *  @param  int $iSectionID   A valid section_id.
	 *  @return array   An assoc. array.
	 *
	 */
	public function getSettings(int $iSectionID = 0): array
	{
        if ($iSectionID == 0)
	    {
	        return [];
	    }
	    
	    $database = LEPTON_database::getInstance();
	    
	    $aSettings = [];
	    $bResult = $database->execute_query(
            "SELECT `header`,`footer`,`format` FROM `" . TABLE_PREFIX . "mod_manual_settings` WHERE `section_id`=" . $iSectionID,
	        true,
	        $aSettings,
	        false
	    );

        if (false === $bResult)
	    {
            echo LEPTON_tools::display($database->get_error());
	    }
	    
	    return $aSettings;
	}

    /**
     * @param string $link
     * @return string
     */
    public function page_link(string $link): string
    {
        // Check for :// in the link (used in URL's) as well as mailto:
        if (strstr($link, '://') == '' && !str_starts_with($link, 'mailto:')) {
            return LEPTON_URL . PAGES_DIRECTORY . $link . PAGE_EXTENSION;
        }
        else {
            return $link;
        }
    }
}
