<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

class manual_env
{
    const MANUAL_TYPES = [
        "HTML",
        "MarkDown",
        "RAW"
    ];
    
    const MANUAL_ISRAW      = 0x00;
    const MANUAL_ISHTML     = 0x01;
    const MANUAL_ISMARKDOWN = 0x02;
    
    const MANUAL_NONE           = 0x00;
    
    const MANUAL_LIB_PARSEDDOWN = 0x01;
    const MANUAL_LIB_CODEMIRROR = 0x02;
    
    const MANUAL_USE_EDITOR     = true;
    const MANUAL_DEFAULT_TYPE   = "HTML";
    
    static public $instance;
    
    private $codemirror_support = false;
    private $parsedown_support  = false;
    
    static public function getInstance()
    {
        if (null === static::$instance)
        {
            static::$instance = new static();
            static::$instance->initialize();
        }
        return static::$instance;
    }
    
    public function initialize()
    {
    
    }
    
    // finalize
    final public function __construct() {}
    final public function __destruct() {}
    final public function __SET( $a, $b ) { echo "inval. call of ".__CLASS__." :: __set"; }
    final public function __GET( $a ) { echo "inval. call of ".__CLASS__." :: __get"; }

}