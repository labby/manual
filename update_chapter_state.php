<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


//  File is called by js via ajax

header('Content-Type: application/javascript');

if (!isset($_POST['cid']))
{
    die("E1");
}

if (!isset($_POST['sig']))
{
    die("E0");
}

$iChapterID = intval(trim($_POST['cid']));

if ($iChapterID < 1)
{
    die("E2");
}

$database = LEPTON_database::getInstance();

$chapterInfo = [];
$database->execute_query(
    "SELECT `active`, `title` FROM `" . TABLE_PREFIX . "mod_manual_chapters` WHERE `chapter_id`=" . $iChapterID,
    true,
    $chapterInfo,
    false
);

$iActualState = $chapterInfo['active'] ?? null;

if ((null === $iActualState) || (false === $iActualState))
{
    die("E[4]= " . $database->get_error());
} else {
    $iActualState = intval($iActualState);
    $iActualState = ($iActualState == 1) ? 0 : 1;

    $bResult = $database->simple_query("UPDATE `" . TABLE_PREFIX . "mod_manual_chapters` set `active`=" . $iActualState . " WHERE `chapter_id`=" . $iChapterID);

    if (false === $bResult)
    {
        die("E[5]= " . $database->get_error());
    }
}

$newState = ["inactive","active"];
echo "Active state of '" . $chapterInfo['title'] . "' has been successfully changed to " . $newState[$iActualState] . ".~" . $iActualState . "\n";
