# Manual

## Description
This is an addon for LEPTON-CMS to create a manual.

## Require
- LEPTON-CMS 6.0

## Support/Bugs
Please report bugs on the LEPTON Addon Forum, where also support is available
http://forum.lepton-cms.org/

## Download
Current installable release can be downloaded on
http://www.lepton-cms.com/lepador/modules/manual.php

## Changelog
Detailed Changelog can be seen on
https://gitlab.com/labby/manual

## Droplet

A handy droplet can be

```php
if(!isset($id)) return "";

$oManual = manual::getInstance();
return $oManual->get_root_link( $id );
```

Example given (droplet named ”manual_link“):

```html
Any place inside [[manual_link?id=14]]
```