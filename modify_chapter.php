<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


// Get id
if (!isset($_GET['chapter_id']) or !is_numeric($_GET['chapter_id']))
{
    header("Location: " . ADMIN_URL . "/pages/index.php");
} else {
	$chapter_id = ($_GET['chapter_id']);
}

// Include WB admin wrapper script
require LEPTON_PATH.'/modules/admin.php';

$oManual = manual::getInstance();
$all_chapters = $oManual->get_manual_by_sectionID($section_id);

if (0 == $chapter_id)
{
    $actual_chapter_content = [
        "content"       => "",
        "parent"        => 0,
        "title"         => "",
        "description"   => "",
        "link"          => "",
        "position"      => 0,
        "active"        => 1,
        "modified_by"   => 0, // !
        "modified_when" => 0,
        "see_also"      => "",
        "type"          => 1,    // ! be careful
    ];
    
} else {
    // Get current values for this chapter-id
    $actual_chapter_content = $all_chapters[ $chapter_id ];
}

if(isset($_SESSION['manual_chapter_last_values']))
{
    $aTemp = ["title","description","content","type","active", "parent"];
    foreach( $aTemp as $key )
    {
        $actual_chapter_content[ $key ] = $_SESSION['manual_chapter_last_values'][ $key ];
    }
    // unset( $_SESSION['manual_chapter_last_values']);
}

$content = (htmlspecialchars($actual_chapter_content['content']));

if (    (!defined('WYSIWYG_EDITOR') ) 
    ||  ( WYSIWYG_EDITOR=="none" ) 
    ||  (!file_exists(LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php'))
    ||  ($actual_chapter_content['type'] != 1)
    )
    {
	    function show_wysiwyg_editor($name,$id,$content,$width,$height) {
		    return '<textarea name="'.$name.'" id="'.$id.'" style="width: '.$width.'%; height: '.$height.'px;">'.$content.'</textarea>';
	    }
	    
} else {
	$id_list=array("content");
	require(LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php');
	
	if(class_exists("tinymce", true))
	{
	    tinymce::getInstance()->selector = "textarea[id=manual_content]";
	}
}
// include jscalendar-setup
$jscal_use_time = true; // whether to use a clock, too

lib_comp::initJsCalendar();
lib_comp::JsCalendarsSetup();

$leptoken = get_leptoken();

$image_base_url = LEPTON_URL."/templates/talgos";

/**
 *  Experimental stuff
 *
 *  https://fomantic-ui.com/collections/form.html
 *
 */
$aAllUsers = [];
$database->execute_query(
    "SELECT `user_id`, `username`, `display_name` FROM `".TABLE_PREFIX."users` ORDER BY `display_name`",
    true,
    $aAllUsers,
    true 
);

$aLayout = [
    "page_id"       => [ "type" => "hidden",    "value" => $page_id ],
    "section_id"    => [ "type" => "hidden",    "value" => $section_id ],
    "leptoken"      => [ "type" => "hidden",    "value" => $leptoken ],
    "chapter_id"    => [ "type" => "hidden",    "value" => $chapter_id ],
    "active"        => [ "type" => "hidden",    "value" => 0 ],
    "modified_when" => [ "type" => "text",      "transform"  => "date", "required"  => 1 ]
];

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule("manual");

//$aAllChapters = $oManual->getAllChapters();

$chapter_tree = [];
$oManual->build_backend_tree($all_chapters, $chapter_tree, 0);
// echo LEPTON_tools::display($chapter_tree, "pre", "ui message green");

echo $oTWIG->render(
    "@manual/backend/modify_chapter.lte",
    [
        "page_id"       => $page_id,
        "section_id"    => $section_id,
        "leptoken"      => $leptoken,
        "allUsers"      => $aAllUsers,
        "oManual"       => $oManual,
        "wysiwyg"       => show_wysiwyg_editor("content","manual_content",$content,"100","400", false),
        //"allChapters"   => $aAllChapters,
        "chapter_tree"  => $chapter_tree,
        "aSeeAlsoIDs"   => explode(",", $actual_chapter_content['see_also']),
        "actual_chapter_content"    => $actual_chapter_content,
        "all_chapters"              => $all_chapters,
        "chapter_id"                => $chapter_id
    ]
);

// Print admin footer
$admin->print_footer();
