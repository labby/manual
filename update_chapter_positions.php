<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


//  File is called by js via ajax

header('Content-Type: application/javascript');

if(!isset($_POST['chapters']))
{
    die("E1");
}

if(!isset($_POST['sig']))
{
    die("E0");
}

$sSectionList = trim( $_POST['chapters'] );

if( $sSectionList == "" )
{
    die("E2");
}

$aSections = explode(",", $sSectionList);
$aNewList = array();
foreach($aSections as $ref)
{
    if($ref != "") $aNewList[] = intval($ref);
}

if(0 === count($aNewList))
{
    return "Error [2]: no Items in list.";
}

$errors = array();
$position = 1;

$database = LEPTON_database::getInstance();

foreach($aNewList as $chapter_id)
{
    $fields = array(
        'position'  => $position++
    );
    
    $database->build_and_execute(
        'update',
        TABLE_PREFIX."mod_manual_chapters",
        $fields,
        "`chapter_id`=".$chapter_id
    );

    if ($database->is_error())
    {
        $errors[]=$database->get_error();
    }
}

if( 0 < count($errors) )
{
    echo "Error [3]: ". implode("\n", $errors);

} else {

    echo "Order has been successfully changed: ".json_encode( $aNewList ).".\n";

}
