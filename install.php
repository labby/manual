<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$database = LEPTON_database::getInstance();
    
$database->simple_query("DROP TABLE IF EXISTS `".TABLE_PREFIX."mod_manual_chapters`");
$mod_manual_chapters = "
CREATE TABLE `".TABLE_PREFIX."mod_manual_chapters` (
	`chapter_id`  int(11) NOT NULL AUTO_INCREMENT,
	`section_id`  int(11) NOT NULL DEFAULT 0,
	`page_id`     int(11) NOT NULL DEFAULT 0,
	`active`      int(11) NOT NULL DEFAULT 0,
	`parent`      int(11) NOT NULL DEFAULT 0,
	`root_parent` int(11) NOT NULL DEFAULT 0,
	`level`       int(11) NOT NULL DEFAULT 0,
	`position`    int(11) NOT NULL DEFAULT 0,
	`link`        text NOT NULL,
	`title`       varchar(255) NOT NULL DEFAULT '',
	`description` text NOT NULL,
	`content`     text NOT NULL,
	`modified_when`   int(11) NOT NULL DEFAULT 0,
	`modified_by`     int(11) NOT NULL DEFAULT 0,
	`tags`        text,
	`see_also`    varchar(255) DEFAULT '',
	`type`        INT NOT NULL DEFAULT 1,
	PRIMARY KEY (`chapter_id`)
)";
$database->simple_query($mod_manual_chapters);

$database->simple_query("DROP TABLE IF EXISTS `".TABLE_PREFIX."mod_manual_settings`");
$mod_manual_settings = "
CREATE TABLE `".TABLE_PREFIX."mod_manual_settings` (
`setting_id`  int(11) NOT NULL AUTO_INCREMENT,
`section_id`  int(11) NOT NULL DEFAULT 0,
`page_id`     int(11) NOT NULL DEFAULT 0,
`header`      text NOT NULL,
`footer`      text NOT NULL,
`format`      text NOT NULL,
PRIMARY KEY (`setting_id`)
)	
";
$database->simple_query($mod_manual_settings);


// Insert info into the search table
// Module query info
$field_info = array();
$field_info['page_id'] = 'page_id';
$field_info['title'] = 'page_title';
$field_info['link'] = 'link';
$field_info = serialize($field_info);
$database->simple_query("INSERT INTO ".TABLE_PREFIX."search (name,value,extra) VALUES ('module', 'manual', '$field_info')");
// Query start
$query_start_code = "SELECT [TP]pages.page_id, [TP]pages.page_title,	[TP]pages.link	FROM [TP]mod_manual_chapters, [TP]pages WHERE ";
$database->simple_query("INSERT INTO ".TABLE_PREFIX."search (name,value,extra) VALUES ('query_start', '$query_start_code', 'manual')");
// Query body
$query_body_code = "
[TP]pages.page_id = [TP]mod_manual_chapters.page_id AND [TP]mod_manual_chapters.title [O] \'[W][STRING][W]\' AND [TP]pages.searching = \'1\'
OR [TP]pages.page_id = [TP]mod_manual_chapters.page_id AND [TP]mod_manual_chapters.description [O] \'[W][STRING][W]\' AND [TP]pages.searching = \'1\'
OR [TP]pages.page_id = [TP]mod_manual_chapters.page_id AND [TP]mod_manual_chapters.content [O] \'[W][STRING][W]\' AND [TP]pages.searching = \'1\'";
$database->simple_query("INSERT INTO ".TABLE_PREFIX."search (name,value,extra) VALUES ('query_body', '$query_body_code', 'manual')");
// Query end
$query_end_code = "";	
$database->simple_query("INSERT INTO ".TABLE_PREFIX."search (name,value,extra) VALUES ('query_end', '$query_end_code', 'manual')");
	