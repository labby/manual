<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

$MOD_MANUAL = array(
    "action"        => "Action",
    "name"          => "Name",
    "description"   => "Description",
    "active"        => "Active",
    "down"          => "Down",
    "up"            => "Up",
    "CHAPTERS"      => "capitolo",
    "add_chapter"   => "Aggiungi un nuovo capitolo",
    "INDEX"         => "indice",
    "LASTUPDATED"   => "ultima modifica da",
    "ON"            => "il",
    "AT"            => "alle",
    "UNDER CONSTRUCTION"    => "in costruzione",
    "STYLE"         => "Manual Style",
    "ARE_YOU_SURE"  => "Sei sicuro di voler eliminare il capitolo %s?",
    "TAGS"          => "Contrassegnare",
    "SEE_ALSO"      => "Guarda anche",
    "types"         => [
            manual_env::MANUAL_ISRAW    => "Raw",
            manual_env::MANUAL_ISHTML   => "HTML",
            manual_env::MANUAL_ISMARKDOWN => "MarkDown"
        ],
    "type"          => "Type",
    "missing"       => [
        "title"         => "Title is missing. A chapter without a title doen't make sense.",
        "content"       => "Content is missing. Nothing to say or write?",
        "description"   => "There is no description for this chapter!" 
    ]
);
