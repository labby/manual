<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

$MOD_MANUAL = array(
    "action"            => "Aktion",
    "name"              => "Name",
    "description"       => "Beschreibung",
    "active"            => "Aktiv",
    "down"              => "runter",
    "up"                => "hoch",
    "CHAPTERS"          => "Kapitel",
    "add_chapter"       => "Kapitel hinzufügen",
    "INDEX"             => "Übersicht",
    "LASTUPDATED"       => "Zuletzt geändert von",
    "ON"                => "am",
    "AT"                => "um",
    "UNDER CONSTRUCTION" => "In Bearbeitung",
    "STYLE"             => "Layout",
    "ARE_YOU_SURE"      => "Wollen Sie wirklich das Kapitel »%s« löschen?\n\nDas kann nicht widerufen werden!",
    "TAGS"              => "Tags",
    "SEE_ALSO"          => "Siehe auch",
    "types"         => [
            manual_env::MANUAL_ISRAW    => "Raw",
            manual_env::MANUAL_ISHTML   => "HTML",
            manual_env::MANUAL_ISMARKDOWN => "MarkDown"
        ],
    "type"          => "Type",
    "missing"       => [
        "title"         => "Title is missing. A chapter without a title doen't make sense.",
        "content"       => "Content is missing. Nothing to say or write?",
        "description"   => "There is no description for this chapter!" 
    ]
);
