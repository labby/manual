<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

$MOD_MANUAL = array(
    "action"            => "Action",
    "name"              => "Name",
    "description"       => "Description",
    "active"            => "Active",
    "down"              => "Down",
    "up"                => "Up",
    "CHAPTERS"          => "Kapitel",
    "add_chapter"       => "Add a chapter",    
    "INDEX"             => "Indeks",
    "LASTUPDATED"       => "Sidst opdateret af",
    "ON"                => "den",
    "AT"                => "kl.",
    "UNDER CONSTRUCTION" => "Under Konstruktion",
    "STYLE"             => "Manual Style",
    "ARE_YOU_SURE"      => "Are you sure you want to delete the chapter “%s”? This cannot be undone!",
    "TAGS"              => "Tags",
    "SEE_ALSO"          => "see also",
    "types"         => [
            manual_env::MANUAL_ISRAW    => "Raw",
            manual_env::MANUAL_ISHTML   => "HTML",
            manual_env::MANUAL_ISMARKDOWN => "MarkDown"
        ],
    "type"          => "Type",
    "missing"       => [
        "title"         => "Title is missing. A chapter without a title doen't make sense.",
        "content"       => "Content is missing. Nothing to say or write?",
        "description"   => "There is no description for this chapter!" 
    ]
);
