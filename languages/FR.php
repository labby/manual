<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */
$MOD_MANUAL    = array(
    "action"            => "Action",
    "name"              => "Name",
    "description"       => "Description",
    "active"            => "Active",
    "down"              => "Down",
    "up"                => "Up",
    "CHAPTERS"          => "chapitre",
    "add_chapter"       => "Ajouter un nouveau chapitre",    
    "INDEX"             => "sommaire",
    "LASTUPDATED"       => "derniere modification par",
    "ON"                => "le",
    "AT"                => "a",
    "UNDER CONSTRUCTION" => "en construction",
    "STYLE"             => "Manual Style",
    "ARE_YOU_SURE"      => "Êtes-vous sûr de vouloir supprimer le chapitre \n\n%s?\n\nÇa ne peut pas être annulé!",
    "TAGS"              => "l'étiquette",
    "SEE_ALSO"          => "Voir également",
    "types"         => [
            manual_env::MANUAL_ISRAW    => "Raw",
            manual_env::MANUAL_ISHTML   => "HTML",
            manual_env::MANUAL_ISMARKDOWN => "MarkDown"
        ],
    "type"          => "Type",
    "missing"       => [
        "title"         => "Title is missing. A chapter without a title doen't make sense.",
        "content"       => "Content is missing. Nothing to say or write?",
        "description"   => "There is no description for this chapter!" 
    ]
);

?>