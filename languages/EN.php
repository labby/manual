<?php

/**
 *  @module         manual
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2004-2023 Ryan Djurovich, Matthias Gallas, Uffe Christoffersen, pcwacht, Rob Smith, Aldus, erpe
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */
$MOD_MANUAL = array(
    "action"        => "Action",
    "name"          => "Name",
    "description"   => "Description",
    "active"        => "Active",
    "down"          => "Down",
    "up"            => "Up",
    "CHAPTERS"      => "Chapters",
    "add_chapter"   => "Add a chapter",
    "INDEX"         => "Index",
    "LASTUPDATED"   => "Last updated by",
    "ON"            => "on",
    "AT"            => "at",
    "UNDER CONSTRUCTION" => "Under Construction",
    "STYLE"         => "Manual Style",
    "SETTINGS"      => "Manual Settings",
    "ARE_YOU_SURE"    => "Are you really sure you want\nto delete the chapter\n\n%s?\n\nThis cannot be undone!",
    "TAGS"          => "Tags",
    "SEE_ALSO"      => "see also",
    "types"         => [
            manual_env::MANUAL_ISRAW    => "Raw",
            manual_env::MANUAL_ISHTML   => "HTML",
            manual_env::MANUAL_ISMARKDOWN => "MarkDown"
        ],
    "type"          => "Type",
    "missing"       => [
        "title"         => "Title is missing. A chapter without a title doesn't make sense.",
        "content"       => "Content is missing. Nothing to say or write?",
        "description"   => "There is no description for this chapter!" 
    ]
);

